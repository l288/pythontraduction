from queue import Empty
from PIL import Image
from googletrans import Translator
from langdetect import detect, DetectorFactory, detect_langs
import pytesseract
import PySimpleGUI as sg

sg.theme('DarkAmber')

output = sg.Text()
layout = [  [sg.Text('Traduction')],
            [sg.Button('Quitter'), sg.Submit('Traduire'), sg.Button('Image'),  sg.InputText(key='textATrad')],
            [sg.Text('Texte traduit: '), output],
            [sg.Text('Langue de destination')],
            [sg.Listbox(values=['fr', 'ko'], size=(10, 10))] ]

window = sg.Window('Window Title', layout, size=(500, 400))

while True:
    event, values = window.read()
    if event == sg.WIN_CLOSED or event == 'Quitter':
        break

    if event == 'Traduire':
        #Choix de la langue de destination
        langChoisi = values[0]
        langDest = langChoisi[0].replace('[', "")
        translator = Translator()
        traduction = translator.translate(values['textATrad'], src=detect(values['textATrad']), dest=langDest)
        trad = traduction.text
        output.update(value=trad)

#Traduction d'une image
    if event == 'Image':
        path = sg.popup_get_file('')
        #resultat2 = pytesseract.image_to_string(Image.open(path), lang='kor')
        resultat = pytesseract.image_to_string(Image.open(path), lang='Hangul')
        result = resultat.replace("\n\x0c", "")
        translator = Translator()
        traduction = translator.translate(result, src='ko', dest='fr')
        output.update(value=traduction.text)

window.close()

